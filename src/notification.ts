enum Notification {
  lint = 'mslint/lint',
  initConfig = 'mslint/init-config',
  updateConfig = 'mslint/update-config',
  resetConfig = 'mslint/reset-config',
  invalidConfig = 'mslint/invalid-config'
}

export {
  Notification
}
