import { stat } from 'node:fs/promises'
import { Stats } from 'node:fs'
import { parse as parsePath } from 'node:path'
import {
  commands,
  window,
  workspace,
  Uri,
  type ExtensionContext,
  RelativePattern,
  type FileSystemWatcher,
  type FileStat
} from 'vscode'
import {
  LanguageClient,
  type LanguageClientOptions,
  type ServerOptions,
  TransportKind
} from 'vscode-languageclient/node'
import { Notification } from './notification'

interface InvalidConfigNotification {
  configFilePath: string
  message: string
}

let client: LanguageClient | undefined
let configFileWatcher: FileSystemWatcher | undefined

function getConfigFileName(): string {
  return workspace.getConfiguration('mslint').get<string>('configFileName', 'mslint.json')
}

function getConfigFilePath(): string {
  return workspace.getConfiguration('mslint').get<string>('configFilePath', '')
}

function createConfigFileWatcher(client: LanguageClient): FileSystemWatcher | undefined {
  let watcher: FileSystemWatcher | undefined
  let fileCheck: Thenable<Stats | FileStat> | undefined
  let configFileUri: Uri = Uri.file('')
  const configFilePath = getConfigFilePath()
  const configFileName = getConfigFileName()

  if (configFilePath !== '') {
    const parsedPath = parsePath(configFilePath)
    watcher = workspace.createFileSystemWatcher(new RelativePattern(Uri.file(parsedPath.dir), parsedPath.base))
    configFileUri = Uri.file(configFilePath)
    fileCheck = stat(configFilePath)
  } else if (workspace.workspaceFolders !== undefined && workspace.workspaceFolders.length === 1 && configFileName !== '') {
    watcher = workspace.createFileSystemWatcher(
      new RelativePattern(workspace.workspaceFolders[0], getConfigFileName())
    )
    configFileUri = Uri.joinPath(workspace.workspaceFolders[0].uri, getConfigFileName())
    fileCheck = workspace.fs.stat(configFileUri)
  }

  if (watcher !== undefined) {
    watcher.onDidCreate(uri => {
      void client.sendNotification(Notification.initConfig, uri)
    })
    watcher.onDidChange(uri => {
      void client.sendNotification(Notification.updateConfig, uri)
    })
    watcher.onDidDelete(() => {
      void client.sendNotification(Notification.resetConfig)
    })
  }

  if (fileCheck !== undefined) {
    fileCheck.then(
      () => {
        void client.sendNotification(Notification.initConfig, configFileUri)
      },
      () => {
        void client.sendNotification(Notification.resetConfig)
        client.warn('MSLint did not find a config file')
      }
    )
  }

  return watcher
}

function watchConfigFile(client: LanguageClient) {
  if (configFileWatcher !== undefined) {
    configFileWatcher.dispose()
  }
  configFileWatcher = createConfigFileWatcher(client)
}

function createMSLintClient(context: ExtensionContext): LanguageClient {
  // Server options
  const serverPath: string = Uri.joinPath(context.extensionUri, 'dist', 'server.cjs').fsPath
  const serverOptions: ServerOptions = {
    run: {
      module: serverPath,
      transport: TransportKind.ipc
    },
    debug: {
      module: serverPath,
      transport: TransportKind.ipc,
      options: {
        execArgv: ['--nolazy', '--inspect=6012']
      }
    }
  }

  // Client options
  const clientOptions: LanguageClientOptions = {
    documentSelector: [
      { scheme: 'file', language: 'maniascript' }
    ],
    diagnosticCollectionName: 'mslint'
  }

  return new LanguageClient(
    'MSLint',
    serverOptions,
    clientOptions
  )
}

async function activate(context: ExtensionContext): Promise<void> {
  // Create MSLint Language Client
  client = createMSLintClient(context)

  // Commands
  context.subscriptions.push(
    commands.registerCommand('mslint.lint', () => {
      if (window.activeTextEditor !== undefined) {
        void client?.sendNotification(Notification.lint, window.activeTextEditor.document.uri.toString())
      }
    })
  )

  // Reload the extension if the `Config File Name` setting was updated
  context.subscriptions.push(
    workspace.onDidChangeConfiguration(change => {
      if (
        client !== undefined && (
          change.affectsConfiguration('mslint.configFileName') ||
          change.affectsConfiguration('mslint.configFilePath')
        )
      ) {
        watchConfigFile(client)
      }
    })
  )

  // Start client
  try {
    await client.start()

    // Watch configuration file
    watchConfigFile(client)

    // Server notifications
    context.subscriptions.push(
      client.onNotification(
        Notification.invalidConfig,
        ({ configFilePath, message }: InvalidConfigNotification) => {
          client?.error(`Invalid configuration file '${configFilePath}'`, message)
        }
      )
    )
  } catch (error) {
    client.error('Failed to start MSLint server', error, 'force')
  }
}

function deactivate(): Promise<void> {
  if (configFileWatcher !== undefined) {
    configFileWatcher.dispose()
  }
  return client !== undefined ? client.stop() : Promise.resolve()
}

export {
  activate,
  deactivate
}
