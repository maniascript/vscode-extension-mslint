import {
  createConnection,
  type Connection,
  DidChangeConfigurationNotification,
  type InitializeParams,
  type InitializeResult,
  ProposedFeatures,
  type ServerCapabilities,
  TextDocuments,
  TextDocumentSyncKind,
  Range,
  Diagnostic,
  DiagnosticSeverity,
  DiagnosticTag,
  type ClientCapabilities,
  type DidChangeConfigurationParams
} from 'vscode-languageserver/node'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { URI } from 'vscode-uri'
import { Linter, type LinterFileReport, Emitter, rules, config } from '@maniascript/mslint'
import { Notification } from './notification'

const VALIDATIONQUEUE_TIMEOUT = 500
const LINTER_DEFAULT_CONFIG = config.validateConfig({
  linter: ['mslint:recommended']
})

class ValidationQueue {
  queue: Map<string, NodeJS.Timeout>

  constructor () {
    this.queue = new Map<string, NodeJS.Timeout>()
  }

  add (uri: string) {
    this.remove(uri)
    this.queue.set(
      uri,
      setTimeout(() => {
        this.queue.delete(uri)
        void validateDocument(uri)
      }, VALIDATIONQUEUE_TIMEOUT)
    )
  }

  remove (uri: string) {
    const timeout: NodeJS.Timeout | undefined = this.queue.get(uri)
    if (timeout !== undefined) {
      clearTimeout(timeout)
    }
  }
}

enum LintOn {
  type = 'type',
  save = 'save',
  command = 'command'
}

interface MSLintConfiguration {
  lintOn: LintOn
}

const MSLINT_DEFAULT_CONFIG: MSLintConfiguration = {
  lintOn: LintOn.type
}

class Configuration {
  #clientHasConfigurationCapability: boolean
  #clientConfiguration: Map<string, MSLintConfiguration>
  #fallbackConfiguration: MSLintConfiguration

  constructor () {
    this.#clientHasConfigurationCapability = false
    this.#clientConfiguration = new Map()
    this.#fallbackConfiguration = MSLINT_DEFAULT_CONFIG
  }

  setClientCapabilities (capabilities: ClientCapabilities) {
    this.#clientHasConfigurationCapability = capabilities.workspace?.configuration ?? false
  }

  get clientHasConfigurationCapability (): boolean {
    return this.#clientHasConfigurationCapability
  }

  async get (connection: Connection, documentUri: string): Promise<MSLintConfiguration> {
    if (this.#clientHasConfigurationCapability) {
      let documentConfig = this.#clientConfiguration.get(documentUri)
      if (documentConfig === undefined) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        documentConfig = await connection.workspace.getConfiguration({
          scopeUri: documentUri,
          section: 'mslint'
        })
        if (documentConfig === undefined) {
          documentConfig = MSLINT_DEFAULT_CONFIG
        }
        this.#clientConfiguration.set(documentUri, documentConfig)
      }
      return documentConfig
    } else {
      return this.#fallbackConfiguration
    }
  }

  onDidChangeConfiguration (change: DidChangeConfigurationParams) {
    if (this.#clientHasConfigurationCapability) {
      this.#clientConfiguration.clear()
    } else {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
      this.#fallbackConfiguration = change.settings.mslint ?? MSLINT_DEFAULT_CONFIG
    }
  }

  onDidCloseDocument (documentUri: string) {
    this.#clientConfiguration.delete(documentUri)
  }
}

function formatDiagnostics(result: LinterFileReport, document: TextDocument): Diagnostic[] {
  const diagnostics: Diagnostic[] = []

  for (const message of result.messages) {
    if (message.severity !== config.Severity.Off) {
      const diagnostic: Diagnostic = Diagnostic.create(
        Range.create(
          document.positionAt(message.source.range.start),
          document.positionAt(message.source.range.end + 1)
        ),
        message.message,
        message.emitter === Emitter.Parser ? DiagnosticSeverity.Error : DiagnosticSeverity.Warning,
        message.emitter === Emitter.Parser ? 'parser-error' : (message.ruleId ?? undefined),
        'mslint'
      )

      diagnostic.codeDescription = {
        href: `https://mslint.aessi.dev/rules/${message.ruleId ?? ''}`
      }

      if (
        message.ruleId === rules.noUnusedExpression.meta.id ||
        message.ruleId === rules.noUnusedInclude.meta.id ||
        message.ruleId === rules.noUnusedVariables.meta.id
      ) {
        diagnostic.tags = [DiagnosticTag.Unnecessary]
      } else if (
        message.ruleId === rules.deprecatedInitializerType.meta.id
      ) {
        diagnostic.tags = [DiagnosticTag.Deprecated]
      }

      diagnostics.push(diagnostic)
    }
  }

  return diagnostics
}

async function validateDocument(uri: string): Promise<void> {
  const document: TextDocument | undefined = documents.get(uri)
  if (document !== undefined) {
    try {
      const documentURI = URI.parse(document.uri)
      const filePath = (documentURI.scheme === 'file') ? documentURI.fsPath : ''
      const result = await linter.lintFile(filePath, document.getText())
      await connection.sendDiagnostics({ uri: document.uri, diagnostics: formatDiagnostics(result, document) })
    } catch (err: unknown) {
      const error = err as Error
      connection.console.error(`Failed to lint document '${uri}'`)
      connection.console.error(error.message)
      await connection.sendDiagnostics({ uri: document.uri, diagnostics: [] })
    }
  }
}

const connection: Connection = createConnection(ProposedFeatures.all)
const documents: TextDocuments<TextDocument> = new TextDocuments<TextDocument>(TextDocument)
const validationQueue = new ValidationQueue()
const configuration = new Configuration()
let linter = new Linter(LINTER_DEFAULT_CONFIG)

// Initialization
connection.onInitialize((params: InitializeParams) => {
  configuration.setClientCapabilities(params.capabilities)

  const serverCapabilities: ServerCapabilities = {
    textDocumentSync: TextDocumentSyncKind.Incremental
  }

  const result: InitializeResult = {
    capabilities: serverCapabilities
  }

  return result
})

connection.onInitialized(() => {
  if (configuration.clientHasConfigurationCapability) {
    void connection.client.register(DidChangeConfigurationNotification.type)
  }
})

// Configuration management
connection.onDidChangeConfiguration(change => {
  configuration.onDidChangeConfiguration(change)
})

// MSlint configuration file management
function resetLinterConfig(): void {
  linter = new Linter(LINTER_DEFAULT_CONFIG)
  for (const document of documents.all()) {
    validationQueue.add(document.uri)
  }
}

function updateLinterConfig(configFileUri: URI): void {
  try {
    linter = new Linter(configFileUri.fsPath)
    for (const document of documents.all()) {
      validationQueue.add(document.uri)
    }
  } catch (err: unknown) {
    const error = err as Error
    void connection.sendNotification(Notification.invalidConfig, { configFilePath: configFileUri.fsPath, message: error.message })
    resetLinterConfig()
  }
}

connection.onNotification(Notification.initConfig, (configFileUri: URI) => {
  updateLinterConfig(URI.from(configFileUri))
})

connection.onNotification(Notification.updateConfig, (configFileUri: URI) => {
  updateLinterConfig(URI.from(configFileUri))
})

connection.onNotification(Notification.resetConfig, () => {
  resetLinterConfig()
})

// Documents management
documents.onDidClose(event => {
  validationQueue.remove(event.document.uri)
  void connection.sendDiagnostics({ uri: event.document.uri, diagnostics: [] })
  configuration.onDidCloseDocument(event.document.uri)
})

documents.onDidChangeContent(async event => {
  const documentConfig = await configuration.get(connection, event.document.uri)
  if (documentConfig.lintOn === LintOn.type) {
    validationQueue.add(event.document.uri)
  }
})

documents.onDidOpen(event => {
  validationQueue.add(event.document.uri)
})

documents.onDidSave(async event => {
  const documentConfig = await configuration.get(connection, event.document.uri)
  if (documentConfig.lintOn === LintOn.save) {
    validationQueue.add(event.document.uri)
  }
})

// Commands
connection.onNotification(Notification.lint, (documentUri: string) => {
  const document = documents.get(documentUri)
  if (document !== undefined) {
    validationQueue.remove(document.uri)
    void validateDocument(document.uri)
  }
})

// Start listening
documents.listen(connection)
connection.listen()
