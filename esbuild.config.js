import esbuild from 'esbuild'

const production = process.argv.includes('--production')
const watch = process.argv.includes('--watch')

/**
 * @type {import('esbuild').Plugin}
 */
const esbuildProblemMatcherPlugin = {
  name: 'esbuild-problem-matcher',
  setup(build) {
    build.onStart(() => {
      console.log('[watch] build started')
    })
    build.onEnd(result => {
      result.errors.forEach(({ text, location }) => {
        console.error(`✘ [ERROR] ${text}`)
        console.error(`    ${location.file}:${location.line}:${location.column}:`)
      })
      console.log('[watch] build finished')
    })
  }
}

const configCommon = {
  platform: 'node',
  format: 'cjs',
  bundle: true,
  minify: production,
  sourcemap: !production,
  sourcesContent: false,
  logLevel: 'silent',
  plugins: [
    esbuildProblemMatcherPlugin
  ]
}

const configClient = {
  ...configCommon,
  entryPoints: ['./src/extension.ts'],
  outfile: './dist/extension.cjs',
  external: ['vscode']
}

const configServer = {
  ...configCommon,
  entryPoints: ['./src/server.ts'],
  outfile: './dist/server.cjs'
}

async function main() {
  const ctxClient = await esbuild.context(configClient)
  const ctxServer = await esbuild.context(configServer)
  if (watch) {
    await ctxClient.watch()
    await ctxServer.watch()
  } else {
    await ctxClient.rebuild()
    await ctxClient.dispose()
    await ctxServer.rebuild()
    await ctxServer.dispose()
  }
}

main().catch(e => {
  console.error(e)
  process.exit(1)
})
